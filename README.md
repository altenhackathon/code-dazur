### Code d'Azur

Iscriviti qui / Subscribe here:
https://www.eventbrite.com/e/alten-hackathon-tickets-59122488956

Sei un tech fan e amante del mare?
Questa e' la tua opportunita'!!
Alten ha organizzato a Pavia una giornata evento dove diversi team si sfideranno a colpi di codice.
I migliori avranno la possibilita' di visitare il Polo Tecnologico di Sophia Antipolis (Costa Azzurra) considerata la Silicon Valley europea e di poter lavorare per Amadeus, leader globale nell'ambito travel webservices.

Chi puo' partecipare:
- Studenti iscritti all'ultimo anno di un corso di Laurea Magistrale in facolta' informatiche/ingegneristiche.
- Neolaureati ad un corso di Laurea Magistrale in facolta' informatiche/ingegneristiche.
- Sviluppatori con esperienza

Premi in palio:
1. Membri del team vincitore:
- Soggiorno in Costa Azzurra (3 giorni)
- Ultimo step del processo di selezione con Alten
2. Membri del team secondo classificato :
- Colloquio diretto con il business manager Alten Mercoledi 22 Maggio durante il carrier day di Pavia.
- Amazon echo dot speaker
3. Membri del team terzo classificato:
- Alten speaker bluetooth a led
- Colloquio diretto con il business manager Alten Mercoledi 22 Maggio durante il carrier day di Pavia.

Perche' dovrei partecipare?
- 1h di Workshop su Agile Methodology tenuto da Scrum Master ceritificati
- Lavorare in team agile su un use case reale

Cosa significa essere assunti da Alten?
Qui alcuni benefici:
- Contesto internazionale con colleghi provenienti da tutto il mondo e di altissimo livello
- Stipendio maggiore rispetto alla media italiana
- Progetti interessanti in diversi linguaggi di programmazione
- Orario lavorativo giornaliero di 7 ore circa
- Corsi di lingua francesi finanziati da Alten
- Sviluppo prodotti in Metodologia Agile
- Vivere sul mare in Costa Azzurra

....PS: il pranzo ve lo offriamo noi :)

-----------------------------------------------------------------------------------

Are you a tech fan and sea lover?
This is your chance !!
Alten organized an event day in Pavia where several teams will challenge each other with code shots.
The best will have the opportunity to visit the Sophia Antipolis (Cote D'Azur) Technology Center, (considered the European Silicon Valley) and be able to work for Amadeus, a global leader in travel web services.

Who can participate:
- Students enrolled in the last year of a master's degree course in information technology / engineering.
- New graduates in a master's degree course in computer science / engineering.
- Experienced developers

Prizes:
1. Winning team members:
- Stay on the French Riviera (3 days)
- Last step of the selection process with Alten
2. Team members ranked second:
- Direct interview with the business manager Alten Wednesday 22 May during the Pavia carrier day.
- Amazon echo dot speaker
3. Third-placed team members:
- Alten bluetooth led speaker
- Direct interview with the business manager Alten Wednesday 22 May during the Pavia carrier day.

Why should I participate?
- 1h of Workshop on Agile Methodology held by certified Scrum Master
- Work in an agile team on a real use case

What does it mean to be hired by Alten?
Here are some benefits:
- International context with high-level colleagues from all over the world
- Salary higher than the Italian average
- Interesting projects in different programming languages
- Daily working time of about 7 hours
- French language courses funded by Alten
- Product development in Agile Methodology
- Living on the sea in the French Riviera

.... PS: we offer the lunch :)